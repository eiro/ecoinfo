---
vim:      et ts=2 sts=2 sw=2
title:    Sobriété et convivialité numérique
keywords: [sobriété numérique, énergie]
author: marc.chantreux chez renater.fr
header-includes: |
  \usepackage{tikz,makecell,epigraph}
  \usepackage{tikzpeople}
  \usetikzlibrary{calc,matrix}
  \setlength \epigraphwidth {\textwidth}
  \setlength \epigraphrule {1pt}
  \renewcommand {\epigraphflush} {center}
  \renewcommand {\sourceflush} {center}
  \newcommand\tpic[1]{\begin{tikzpicture}[remember picture,overlay,ampersand replacement=\&]#1\end{tikzpicture}}

  \newcommand\up[1]{\LARGE\textbf{#1}}

  \newcommand\takahashi[1]{
    \begin{tikzpicture}[remember picture,overlay]
      \node[at=(current page.center)] {\makecell[c]{#1}};
    \end{tikzpicture}
  }

  \newcommand\fsimg[1]{
      \node[at=(current page.center)] {
          \includegraphics[
             keepaspectratio,
             width=\paperwidth,
             height=\paperheight]{#1} };
  }

  \newcommand\img[1]{
    \begin{tikzpicture}[remember picture,overlay]
    \fsimg{#1}
    \end{tikzpicture}
  }

  \newcommand\surimpression[1]{
    \begin{tikzpicture}[remember picture,overlay]
        % \node[rectangle,draw,fill=black!20,font={this is it}]
        \node[rectangle,fill=black!20, opacity=.8]
          at (current page.center)
          {\makecell[c]{#1}};
    \end{tikzpicture}
  }
---

\takahashi{
  \up{la conférence sans video?}\\
  si! il faut garder les graphiques ...
}
#

\takahashi{
  \up{notre} monde \up{industrialisé}\\
  fait face à des crises\\
  \up{environnementales}\\
  et \up{structurelles}\\
  interdépendantes.\\
  (c'est la seule dia pour laquelle j'espère une adhésion initiale)
  \\
}
#

\takahashi{
  \up{nous}\\
  (une petite partie de l'humanité)\\
  provoquons\\
  la \up{6ème extinction de masse}\\
  (au moins 75\% des espèces animales et végétales)\\
  (5ème: Baptistina nous prive de dinosaures)
}
#

\takahashi{
  \LARGE Quantitative risk assessment \\
  \LARGE of the effects of climate change \\
  \LARGE on selected causes of death \\
  \Large 2030s and 2050s \\ \\
  $\approx$ 250 000 décès supplémentaires par an\\ \\
  \includegraphics[keepaspectratio,width=0.3\paperwidth]{images/oms.pdf}
}
#

\takahashi{
  \up{nous}\\
  payerons probablement le tribut moins lourd et\\
  après tout le monde\\
}
#

\img{cumulative-CO2.png}
#

\takahashi{
  penser le numérique de demain,\\
  c'est se donner une \up{idée} du\\
  \up{projet de société}\\
  qu'il accompagne
}
#

\takahashi{
  ce projet doit être\\
  réaliste\\
  viable\\
  souhaitable\\
  moralement acceptable\\
  \\
}
#

\takahashi{c'est répondre à de nombreuses questions qui\\
structurent notre stratégie\\
font toujours débat
}
#

\takahashi{
  \up{réaliste?}\\
  planète B\\
  fin des compétitions commerciales et géopolitiques\\
  une mobilisation d'une responsabilité collective\\
  geo ingénierie\\
  déploiement d'un nucléaire propre (ITER / Tokamak)\\
  transition énergétique crédible\\
  fonte du permafrost\\
  fin de la banquise arctique\\
  arrêt du golf stream\\
  risques pandémiques comme régulateur\\
  changement climatique\\
  bonne idée chez spaceX\\
}
#

\takahashi{
  \up{viable?}\\
  monoculture intensive\\
  Video On Demand\\
  5G\\
  voiture autonome\\
  taille actuelle de la flotte individuelle\\
  aviation\\
  économie mondialisée\\
  éclairage public\\
  smart city\\
  consommation de viande\\
  import de nourriture exotique\\
  ...
}
#

\takahashi{
  \up{souhaitable?}\\
  la monoculture intensive\\
  la 5G\\
  la voiture autonome\\
  régulation du débit des réseaux\\
  principe du pollueur payeur\\
  ...
}
#

\takahashi{
    Géologie, Géographie, Géopolitique, Climatologie, Météorologie,\\
    Physique(s), Histoire, Économie, Psychologie,\\
    Mathématiques, Informatique,\\
    Philosophie,\\
    ...
}
#

\takahashi{
  \up{anticiper} c'est comprendre\\
  tout est imbriqué\\
  les ramifications sont sans fin\\
  on manque de chiffres et de recul\\
  multidisciplinaire par essence\\
  monde scientifique pas prêt\\
  manque de moyens\\
  ...
}
#

\takahashi{
  \up{pour nous tous}\\
  les mesures effectives et modèles prédictifs éprouvés\\
  les dynamiques\\
  les tendances\\
  les ordres de grandeurs\\
  ...
}
#

\takahashi{
  à défaut d'être sur\\
  transparent\\
  honnête\\
  ouvert à la critique constructive\\
}
#

\img{images/exxon-1982/graph-ici.png}
#

\img{leanit/i-230.png}
#

\img{images/GlobalPeakOil.png}
#

\img{images/energy_sources.jpg}
#

\img{images/world3.jpg}
#

\takahashi{
  alimenter un marché c'est\\
  provoquer un désir (demande)\\
  satisfaire ce désir (offre)\\
  \\
  les psys nous disent que le désir est infini\\
  les ressources, elles, sont finies\\
  \\
  comme les désirs sont déjà en place\\
  provoquer la frustration\\
  proposer des alternatives\\
  ressentir un autre bien-être\\
}
#

\takahashi{
  nous consommons de plus en plus\\
  de ressources et d'énergie\\
  pour extraire exploiter\\
  de ressources et d'énergie
}
#

\takahashi{
  Moore
  \\
  souvenirs: explosion de la micro-informatique:\\
  \\
  l'utilité et intérêt\\
  \\
  l'auto-formation était la fréquente\\
  et vecteur de lien social\\
  \\
  explosion du marché\\
  chute des prix\\
  Internet\\
  promotion de l'offre\\
  évolution des GUI?
}
#

\img{original-content/3-generations-de-fora.png}
#

\takahashi{
  Moore vs Jevons: l'effet rebond
  la facture énergétique de l'informatique\\
  progresse de \up{9\%} par an\\
  et représente \up{4\%}\\
  de la consommation mondiale\\
  (shift project contesté)\\
}
#

\img{leanit/i-170.png}
#

\takahashi{Accompagner la \textbf{sobriété} numérique\\
Préparer la \textbf{résilience} numérique \\
Assurer l'\textbf{équité} numérique\\
Questionner la \textbf{pérénité} numérique\\
}
#

\takahashi{
  \up{50 nuances de green}\\
  \\
  business as usual\\
  green washing\\
  croissance verte et éco-conception\\
  informatique de décroissance\\
  informatique post-personnelle\\
  ...
}
#

\takahashi{
  comprendre nos \up{différences}\\
  \\
  trouver des \up{synergies}\\
  entre les 50 nuances de green\\
}
#

\takahashi{
  mettre déployer un numérique respectueux de\\
  \up{l'implication}
  et de
  \up{la capacité}
  de chacun?
  \\
  alors il faut bien qu'on en discute\\
}
#

\takahashi{
  \up{\#KhmersVerts} et \up{\#teamAmish}\\
  contre les \up{égoistes et cyniques}\\
  pasteurs et moutons de la \up{transumance suicidaire}.
  % compte tenu de la sensibilité du sujet,
  % c'est irresponsable, grave,
  % non-professionnel et mesquin.
}
#

\takahashi{\up{"par ou commencer?"}}
#

\takahashi{
  se sentir coupable de culpabiliser\\
  s'assurer des bonnes conditions de la\\
  \up{libération de la parole}\\
  \\
  provoquer des évènements et des contenus pour\\
  \up{sensibiliser}\\
  \\
  provoquer et animer\\
  \up{un débat}\\
  qui soit normatif et sourcé\\
  (conditions de mesure et d'évaluation)\\
  \\
  \up{accueillir}\\
  la position de \up{chacun}\\
  (proposer de nouvelles missions, de nouvelles méthodes de travail?)\\
  \\
}
# Ecoinfo-forum

le canal veille collective et d'échange ouvert à tous les membres de l'ESR

https://listes.services.cnrs.fr/wws/subscribe/ecoinfo-forum

les personnes qui ne savent pas par ou commencer sont les bienvenues!

# GDS CNRS Ecoinfo

* évaluer et documenter les impacts du numérique
* proposer et évaluer des solutions aux problèmes identifiés
* formation et accompagnement

projets et services

* 26 en cours
* 5  en pause
* au moins 7 en rêve

#

\takahashi{
  réagir arbitrairement, c'est potentiellement\\
  \\
  une \up{dégradation}\\
  de la capacité \up{de mobilisation}\\
  des ressources humaines\\
  \\
  une perte \up{d'attractivité}\\
  pour vos structures
}
#

\takahashi{
  ne pas se dédouaner\\
  \\
  ça n'est pas \up{que}\\
  la faute de notre condition\\
  d'\up{homo-economicus}\\
  \\
  notre consommation\\
  notre prise de parole\\
  \\
  hiérarchies, groupes informels, syndicats,\\
  associations (étudiantes)?,\\
  animations, formations,\\
  ...\\
  \\
  notre implication dans les projets professionnels ou citoyens\\
  \\
  l'éducation de nos enfants
}
#

\takahashi{
  ca n'est pas \up{que} politique\\
  \\
  ils ont un pouvoir\\
  nous devons proposer des solutions\\
}
#


\takahashi{"Le CNRS,
conjointement avec la CPU,
incite les laboratoires dont il\\
est tutelle à mettre en place une réflexion\\
sur l'impact environnemental de leurs activités"
\\
-- le CNRS poursuit son engagement
}
#

\takahashi{
  ca n'est pas \up{que}\\
  dans les mains du GAFAM\\
  \\
  Large adoption mais évoluer vers \up{l'intérêt général}\\
  c'est \up{mettre fin}
  à leurs intérêts particuliers.
}
#

\takahashi{
  ca n'est pas \up{que} la faute\\
  des utilisateurs\\
  \\
  ils utilisent l'informatique\\
  que nous avons produit\\
  \\
  les informer et les former\\
  les encourager à des usages que \up{les informaticiens}\\
  avons créé pour satisfaire des modèles économiques et commerciaux\\
  \\
  impliquons les dans l'imagination et la production d'alternatives\\
}
#

\img{images/jobs.jpg}
#

\img{together.pdf}
#

\takahashi{
  "l'impact environnemental de leurs activités"\\
  ce que nous produisons\\
  comment nous le produisons\\
  avec quoi nous le produisons\\
}
#

\img{images/camion.jpg}
#

\img{images/800kg.png}
\tpic{
    \node
    [white,fill=black!50,opacity=.7]
    at ($(current page.east)-(3,-2.5)$)
    {\makecell[l]{creuser de plus en plus profond
    \\ \Huge \textbf{800Kg}}};
}
#

\img{images/sodom.jpg}

# Cycle de vie d'un objet technologique

* Extraction des ressources
* Production / Assemblage / Conditionnement
* Usage
* Fin de vie
    * Décharge
    * Dépollution
    * Démantèlement
    * Incinération
    * Recyclage

* possiblement du transport entre toutes ses phases

$\implies$ des pollutions chimiques et atmosphériques

$\implies$ des destructions d'écosystèmes

Logique à appliquer récursivement pour chaque objet technologique impliqué
dans un de ces processus.

# problèmes

* problème essentiellement pluridisciplinaire
* réévaluation permanente des données et méthodes
* méthodologie en amélioration constante
* peu de constructeurs responsables et transparents (FairPhone)

$\implies$ des estimations et des données partielles

#

\img{images/repartition.jpg}
#

\takahashi{
  \up{sobriété numérique à visée écologique}\\
  \\
  L'informatique a été sobre\\
  \\
  a périmètre et nombre d'utilisateurs constants\\
  moins de ressources humaines\\
  moins de formation et de culture technique\\
  moins d'assistance et d'accompagnement\\
  \\
  en contrepartie\\
  \\
  plus d'objets, plus de code\\
  plus de réponses technologiques au besoin\\
  d'utilisateurs de plus en plus dépendants et formatés\\
}
#

\takahashi{
  Effet rebond et \\
  \up{explosion du digital market}\\
  \\
  avant\\
  informaticien / webmaster\\
  \\
  après\\
  front, back, netop, devop, SRE, architect, RSSI, CHO, ...
}
#

\takahashi{
  \up{les minimalistes}\\
  \\
  sont ceux qui ont souhaité rester des informaticiens\\
  et ont pratiqué (sans le savoir?)\\
  \\
  \up{la convivialité}
  \\
  se retrouvent autour de projets techniques\\
  ne font pas (plus?) la promotion de leurs idées\\
  \\
  trouver des ponts entre ces mondes\\
  est une \up{option}\\
  qu'il nous faut envisager\\
  \\
}
#

\takahashi{
  occupation de l'espace médiatique\\
  \up{40 nuances de green}\\
  \\
  business as usual\\
  green washing\\
  croissance verte\\
  éco-conception "marginale"\\
}
#

\takahashi{
  \up{ma nuance de green}\\
  informatique de décroissance\\
  informatique post-personnelle\\
  \\
  eco conception minimaliste\\
  partir d'une solution conviviale\\
  mesurer chaque ajout ergonomique ou fonctionnel\\
  établir des ponts avec l'eco conception "marginale"\\
  \\
  eco rénovation: optimiser et permettre l'adoption\\
  de l'ensemble des outils pertinents\\
  \\
}
#

\takahashi{
  \up{xml experiment}\\
  \\
  ENIMAG/Ecoinfo propose le sujet à ses élèves
  \\
}
#

\takahashi{
  \up{tty experiment}\\
  \\
  j'ai trouvé\\
  des utilisateurs (produire des retours)\\
  d'autres cultures (Taiwan, 80's, ..)\\
  des idées\\
  \\
  je manque\\
  de temps\\
  de designers/ergonomes
}
#

\takahashi{
réduire la rotation et le nombre des équipements\\
\\
plus de maintenance et de reconditionnement\\
distributions linux dédiées\\
faire pression sur les éditeurs\\
\\
organiser la rotation des personnels\\
sur les terminaux physiques
}
#

\takahashi{produire les conditions d'une\\
Informatique post-personelle\\
\Huge $ \frac{terminal}{personne} < 1 $\\
quid de nos vies numériques?
}
#

\takahashi{\up{Renater}\\
inscrire l'éco-responsabilité dans sa stratégie\\
sensibilisation en cours\\
\\
ACV (Analyse du Cycle de Vie) avec Ecoinfo\\
un segment du backbone\\
les services de messagerie et de visio\\
\\
initier un groupe de travail environnement (merci)
}
#

\takahashi{
j'aurais voulu avoir plus de \up{temps}\\
pour synthétiser chaque idée\\
pour l'exposer plus en détail\\
\\
certaines sont probablement connes mais j'ai\\
été honnête à défaut d'être sur\\
\\
nous manquons de temps\\
et de compétences disponibles\\
emparez vous des sujets\\
rejoignez ecoinfo-forum\\
}
#

\takahashi{
un jour peut-être, l'humanité pourra estimer\\
le nombre de vies sauvées par nos efforts.\\
\\
en attendant et dans le doute\\
j'imagine, je mesure, je teste, je valide, je propose\\
\\
merci
}


